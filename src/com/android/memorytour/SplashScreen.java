package com.android.memorytour;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class SplashScreen extends Activity {

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.splash);

		ImageView imageView = (ImageView) findViewById(R.id.background);
		Animation fadeInAnimation = AnimationUtils.loadAnimation(this,
				R.anim.fade);
		imageView.startAnimation(fadeInAnimation);

		
		new CountDownTimer(7000, 1000) {
			public void onTick(long millisUntilFinished) {
			}
			public void onFinish() {
				finish();
				startActivity(new Intent(getApplicationContext(),
						ItemListActivity.class));
			}
		}.start();
	}
}
