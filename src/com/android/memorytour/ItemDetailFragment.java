package com.android.memorytour;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.memorytour.dummy.DummyContent;
import com.android.memorytour.games.Html5Game;

/**
 * A fragment representing a single Item detail screen. This fragment is either
 * contained in a {@link ItemListActivity} in two-pane mode (on tablets) or a
 * {@link ItemDetailActivity} on handsets.
 */
public class ItemDetailFragment extends Fragment {

	public static final String TAG = "ItemDetailFragment";
	private static final String URL_QUIZ = "http://memorytour.ideable.es/quiz.html";
	private static final String URL_PUZLE = "http://memorytour.ideable.es/puzzle.html";

	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "item_id";

	/**
	 * The dummy content this fragment is presenting.
	 */
	private DummyContent.DummyItem mItem;

	private int gameCode = 0;
	private TextView textDescription;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public ItemDetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments().containsKey(ARG_ITEM_ID)) {
			// Load the dummy content specified by the fragment
			// arguments. In a real-world scenario, use a Loader
			// to load content from a content provider.
			mItem = DummyContent.ITEM_MAP.get(getArguments().getString(
					ARG_ITEM_ID));

		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View rootView = inflater.inflate(R.layout.fragment_item_detail,
				container, false);

		final ImageView background = (ImageView) rootView
				.findViewById(R.id.background);

		// Show the dummy content as text in a TextView.
		if (mItem != null) {
			((TextView) rootView.findViewById(R.id.item_detail))
					.setText(mItem.content);

			gameCode = Integer.parseInt(mItem.id);
			textDescription = (TextView) rootView
					.findViewById(R.id.item_descripcion);

			switch (gameCode) {

			case 1:
				textDescription.setText("Descripción juego 1");
				background.setImageResource(R.drawable.infancia);
				break;

			case 2:
				textDescription.setText("Descripción juego 2");
				background.setImageResource(R.drawable.juventud);
				break;

			case 3:
				textDescription.setText("Descripción juego 3");
				background.setImageResource(R.drawable.adulta);
				break;

			case 4:
				textDescription.setText("Descripción juego 4");
				background.setImageResource(R.drawable.decada);
				break;

			default:
				break;
			}

			ImageButton b = (ImageButton) rootView
					.findViewById(R.id.buttonPlay2);
			b.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {

					Intent intent = new Intent();
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

					switch (gameCode) {

					case 1:

						break;

					case 2:
						intent.setClass(getActivity(), Html5Game.class);
						intent.putExtra("url", URL_PUZLE);
						startActivity(intent);
						break;

					case 3:

						break;

					case 4:
						intent.setClass(getActivity(), Html5Game.class);
						intent.putExtra("url", URL_QUIZ);
						startActivity(intent);
						break;

					default:
						break;
					}

				}
			});
		}

		return rootView;
	}
}
