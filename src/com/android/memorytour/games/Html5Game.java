package com.android.memorytour.games;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.android.memorytour.R;

public class Html5Game extends Activity {

	public static final String TAG = "Quest";

	public static final String PUZZLE = "MEMOTOUR - Un viaje por tus recuerdos";

	private WebView mWebView;
	private String url = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_quest);

		Intent intent = getIntent();
		url = intent.getStringExtra("url");

		mWebView = (WebView) findViewById(R.id.webView1);

		// Asignamos nuestro cliente web :D
		mWebView.setWebViewClient(new LoginWebViewClient());

		// Activo JavaScript :S
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.getSettings().setBuiltInZoomControls(true);
		mWebView.setBackgroundColor(Color.parseColor("#4a4a4a"));
		mWebView.loadUrl(url);

		// setImage ("images/abuelos.jpg")

	}

	private class LoginWebViewClient extends WebViewClient {

		// Para no salir del webview al pinchar enlaces
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			Log.d("Webview", "onPageFinished: " + view.getTitle());

			if (null != mWebView.getTitle()) {

				// Evento cerrar webview
				if (mWebView.getTitle().contains("cerrar")) {
					finish();
				}
			}
		}

		@Override
		public void onPageStarted(WebView view, String url,
				android.graphics.Bitmap favicon) {
			Log.d("Webview", "onPageStarted: " + view.getTitle());
		}

		@Override
		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) {
			Log.e("Webview", "ERROR" + ": " + description);
		}

	}
}
